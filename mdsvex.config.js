import {defineMDSveXConfig as defineConfig} from 'mdsvex';

const config = defineConfig({
	extensions: ['.svelte.md', '.md', '.svx'],

	smartypants: true,
	layout: {
		_: './src/layouts/default.svelte',
	},
	remarkPlugins: [],
	rehypePlugins: [],
});

export default config;
