import {error} from '@sveltejs/kit';

import {getDocumentByUrl} from '$lib/documents';

import type {PageServerLoad} from './$types';

export const load: PageServerLoad = async ({params}) => {
        const doc = getDocumentByUrl('/documents/' + params.path);
        if (doc) {
                const {html} = doc.module.default.render();
	        return {title: doc.title, content: html};
        }

	throw error(404, 'Not found');
};

export const prerender = true;