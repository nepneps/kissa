type Metadata = {
	title: string | undefined;
	[key: string]: any;
};
type DocumentInfo = Metadata & {
	url: string;
	file?: string;
	module?: any;
};

type FilePath = string;
type MdModule = {metadata: Metadata};

const modules: [FilePath, MdModule][] = Object.entries(
	import.meta.glob('../documents/**/*.md', {eager: true})
);

function filePathToUrl(filePath: string): string {
	return filePath
		.replace(/^\.\.\/documents\//, '/documents/')
		.replace(/\/index\./, '/.')
		.replace(/\.md$/, '');
}

function filePathToTitle(filePath: string): string {
	const basename = filePathToUrl(filePath).replace(/.*\//, '');
	const lowercaseWords = basename
		.trim()
		.replace(/\W+/g, ' ')
		.split(/ |\B(?=[A-Z])/)
		.map((word) => word.toLowerCase())
		.join(' ')
		.replace(/[-_]/g, ' ');

	const titleCase = lowercaseWords.replace(/(\w)\S*/g, (txt) =>
		/^(of|for|a|by|in|and|or|as)$/.test(txt)
			? txt
			: txt.charAt(0).toUpperCase() +
			  txt.substring(1).toLowerCase()
	);
	return (
		lowercaseWords.charAt(0).toUpperCase() +
		lowercaseWords.substring(1)
	);
}

function getDocumentInfo(file: string, module: MdModule): DocumentInfo {
	return {
		url: filePathToUrl(file),
		file,
		module,
		...module.metadata,
		title: module.metadata?.title ?? filePathToTitle(file),
	};
}

export function getDocuments(): DocumentInfo[] {
	return modules.map(([file, module]) => getDocumentInfo(file, module));
}

export function getDocumentByUrl(url: string): DocumentInfo | null {
	for (let doc of getDocuments()) {
		if (doc.url == url) {
			return doc;
		}
	}
	return null;
}
